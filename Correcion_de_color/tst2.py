import cv2
import numpy as np

# cargar imagen
img = cv2.imread("C:/Users/icarito/Documents/Correcion_de_color/Resultados/aaa.png")

# convertir a espacio de color HSV
hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

# definir rango de colores verdes
lower_green = (30, 40, 40)
upper_green = (70, 255, 255)

# crear máscara de color verde
mask = cv2.inRange(hsv, lower_green, upper_green)

# aplicar máscara a imagen original
res = cv2.bitwise_and(img, img, mask=mask)

# mostrar imagen resultante
cv2.imshow("Imagen corregida", res)
cv2.waitKey(0)
cv2.destroyAllWindows()
