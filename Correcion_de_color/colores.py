import cv2
import numpy as np
import imutils 
# capture video from camera
cap = cv2.VideoCapture('C:/Users/icarito/Documents/Correcion_de_color/Resultados/Videos_Reserva_Marina_Putemún_corregido/P5_corrected.mp4')
while True:
    _, frame = cap.read()
    frame = imutils.resize(frame,width=640)

    # define range of green color in HSV
    lower_green = (29, 145, 6)
    upper_green = (64, 255, 255)

    # convert frame to BGR to GRAY
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # convert frame from BGR to HSV
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # create mask
    mask = cv2.inRange(hsv, lower_green, upper_green)

    # apply mask to frame
    res = cv2.bitwise_and(frame, frame, mask=mask)

    # create 3 channel image with green color
    green_image = np.zeros((frame.shape[0], frame.shape[1], 3), dtype=np.uint8)
    green_image[:, :, 1] = 255

    # apply mask to green image
    res_green = cv2.bitwise_and(green_image, green_image, mask=mask)

    # add res_green to frame
    final_image = cv2.addWeighted(res, 1, res_green, 1, 0)
    ff_image = cv2.addWeighted(frame, 1, final_image, 1, 0)

    cv2.imshow('tst', ff_image)
    cv2.imshow("Highlighted Green", final_image)
    cv2.imshow("Gris", gray)

    k = cv2.waitKey(5) & 0xFF
    if k == 27:
        break

cv2.destroyAllWindows()
cap.release()